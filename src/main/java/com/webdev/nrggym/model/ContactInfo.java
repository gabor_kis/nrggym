package com.webdev.nrggym.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
public class ContactInfo {

  public enum ContactType {
    PHONE, EMAIL
  }
  
  @GeneratedValue
  @Id
  private Long id;
  
  
  
  @Enumerated(EnumType.STRING)
  private ContactType type;
  
  
  @Size(max = 30)
  private String identifier;

  @Size(max = 50)
  private String description;
  
  public ContactInfo() {
    
  }
  

  public ContactInfo(Long id, ContactType type, String identifier, String description) {
    this.id = id;
    this.type = type;
    this.identifier = identifier;
    this.description = description;
  }


  public Long getId() {
    return id;
  }


  public void setId(Long id) {
    this.id = id;
  }


  public ContactType getType() {
    return type;
  }


  public void setType(ContactType type) {
    this.type = type;
  }


  public String getIdentifier() {
    return identifier;
  }


  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }


  public String getDescription() {
    return description;
  }


  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public String toString() {
    return "ContactInfo [id=" + id + ", type=" + type + ", identifier=" + identifier + ", description=" + description
        + "]";
  }
  
  
  
}
