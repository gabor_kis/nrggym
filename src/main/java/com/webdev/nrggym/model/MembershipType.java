package com.webdev.nrggym.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class MembershipType {
  
  @GeneratedValue
  @Id
  private Long id;
  
  @NotNull
  @Size(min=2, max=10)
  private String name;
  
  private String description;
  
  @NotNull
  @Min(1)
  private int price;
  
  @NotNull
  @Min(0)
  private int duration;
  
  public MembershipType() {
    
  }

  public MembershipType(Long id, String type, String description, int price, int duration) {
    this.id = id;
    this.name = type;
    this.description = description;
    this.price = price;
    this.duration = duration;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String type) {
    this.name = type;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  @Override
  public String toString() {
    return "MembershipType [id=" + id + ", type=" + name + ", description=" + description + ", price=" + price
        + ", duration=" + duration + "]";
  }
  
  
  
  
  

}
