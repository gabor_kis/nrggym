package com.webdev.nrggym.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Membership {
  
  @GeneratedValue
  @Id
  private Long id;
  
  @ManyToOne
  @JoinColumn(name = "MEMBERSHIP_TYPE_ID")  
  private MembershipType type;
  
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  @NotNull
  @Future
  private Date expiryDate;
  
  @NotNull
  @Min(0)
  private int days;
  
  public Membership() {
    
  }

  public Membership(Long id, MembershipType type, Date expiryDate, int days) {
    super();
    this.id = id;
    this.type = type;
    this.expiryDate = expiryDate;
    this.days = days;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public MembershipType getType() {
    return type;
  }

  public void setType(MembershipType type) {
    this.type = type;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public int getDays() {
    return days;
  }

  public void setDays(int days) {
    this.days = days;
  }
  
  
  
}
