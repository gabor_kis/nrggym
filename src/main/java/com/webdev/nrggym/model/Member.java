package com.webdev.nrggym.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name = "members")
public class Member {

  @GeneratedValue
  @Id
  private Long id;
  

  @NotNull
  @Size(min=2, max=30)
  private String firstName;
  
  @NotNull
  @Size(min=2, max=30)
  private String lastName;
  
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  //@Temporal(TemporalType.DATE)
  @NotNull
  private Date birthDate;
  
  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "MEMBERS_ID")
  private List<ContactInfo> contactInfos = new ArrayList<>();
  
  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "MEMBERS_ID")
  @Valid
  private List<Membership> memberships = new ArrayList<>();
  

  public Member() {
    
  }
  
  
  
  public Member(Long id, String firstName, String lastName, Date birthDate, List<ContactInfo> contactInfos,
                List<Membership> memberships) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.contactInfos = contactInfos;
    this.memberships = memberships;
  }



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }
  
  public List<ContactInfo> getContactInfos() {
    return contactInfos;
  }

  public void setContactInfos(List<ContactInfo> contactInfos) {
    this.contactInfos = contactInfos;
  }

  public List<Membership> getMemberships() {
    return memberships;
  }

  public void setMemberships(List<Membership> memberships) {
    this.memberships = memberships;
  }
  
  
  
}
