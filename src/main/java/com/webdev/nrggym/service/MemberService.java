package com.webdev.nrggym.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webdev.nrggym.model.Member;
import com.webdev.nrggym.repository.MemberRepository;

@Service
public class MemberService {

  
  MemberRepository memberRepo;

  @Autowired
  public void setMemberRepo(MemberRepository memberRepo) {
    this.memberRepo = memberRepo;
  }

  public List<Member> getMembers() {    
    return memberRepo.findAll();
  }
  
  public Member getMemberByLastName(String lastName) {
    return memberRepo.findByLastName(lastName);    
  }
  
  public Member getMemberById(Long id) {
    Member m = memberRepo.findById(id);   
    return m;  
  }
  
  
}
