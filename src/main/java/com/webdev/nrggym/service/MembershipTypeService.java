package com.webdev.nrggym.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webdev.nrggym.model.MembershipType;
import com.webdev.nrggym.repository.MembershipTypeRepository;

@Service
public class MembershipTypeService {

  private MembershipTypeRepository membershipTypeRepo;

  @Autowired
  public void setMembershipTypeRepo(MembershipTypeRepository membershipTypeRepo) {
    this.membershipTypeRepo = membershipTypeRepo;
  }
  
  public List<MembershipType> getMembershipTypes() {    
    return membershipTypeRepo.findAll();
  }  
  
  public MembershipType getMembershipTypesByName(String name) {    
    return membershipTypeRepo.findByName(name);
  }
  
  public MembershipType getMembershipTypesById(Long id) {    
    return membershipTypeRepo.findById(id);
  }
  
  
}
