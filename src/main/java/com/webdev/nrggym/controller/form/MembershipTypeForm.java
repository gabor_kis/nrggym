package com.webdev.nrggym.controller.form;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.webdev.nrggym.model.MembershipType;

public class MembershipTypeForm {

  @Valid
  @NotNull
  private MembershipType membershipType;
  
  private String id;
  
  private String name;
  
  private String description;
  
  private String duration;
  
  private String price;
  
  public static enum Action {
    SAVE, NEW, DELETE
  }
  
  @NotNull
  private Action action;

  public MembershipType getMembershipType() {
    return membershipType;
  }

  public void setMembershipType(MembershipType membershipType) {
    this.membershipType = membershipType;
  }

  public Action getAction() {
    return action;
  }

  public void setAction(Action action) {
    this.action = action;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }
  
  
  
  
}
