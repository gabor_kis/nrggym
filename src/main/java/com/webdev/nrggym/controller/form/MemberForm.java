package com.webdev.nrggym.controller.form;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.webdev.nrggym.model.Member;

public class MemberForm {

  @Valid
  @NotNull
  private Member member;
  

  public static enum Action {
    SAVE, NEW, DELETE, ADD_CONTACT, ADD_MEMBERSHIP, BACK
  }
  
  @NotNull
  private Action action;
  
  private String fullName;
  
  @Min(0)
  private Integer deleteContactInfoIndex;
  
  @Min(0)
  private Integer deleteMembershipInfoIndex;
  
  public MemberForm() {
    
  }

  public MemberForm(Member member, Action action, String fullName, Integer deleteContactInfoIndex,
                    Integer deleteMembershipInfoIndex) {
    this.member = member;
    this.action = action;
    this.fullName = fullName;
    this.deleteContactInfoIndex = deleteContactInfoIndex;
    this.deleteMembershipInfoIndex = deleteMembershipInfoIndex;
  }



  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public Action getAction() {
    return action;
  }

  public void setAction(Action action) {
    this.action = action;
  }

  public Integer getDeleteContactInfoIndex() {
    return deleteContactInfoIndex;
  }

  public void setDeleteContactInfoIndex(Integer deleteContactInfoIndex) {
    this.deleteContactInfoIndex = deleteContactInfoIndex;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Integer getDeleteMembershipInfoIndex() {
    return deleteMembershipInfoIndex;
  }

  public void setDeleteMembershipInfoIndex(Integer deleteMembershipInfoIndex) {
    this.deleteMembershipInfoIndex = deleteMembershipInfoIndex;
  }
  
  
  
  
  
}
