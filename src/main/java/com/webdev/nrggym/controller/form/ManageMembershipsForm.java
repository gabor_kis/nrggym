package com.webdev.nrggym.controller.form;

import javax.validation.constraints.NotNull;

import com.webdev.nrggym.model.Member;
import com.webdev.nrggym.model.MembershipType;

public class ManageMembershipsForm {

  
  private Member member;
  
  
  private MembershipType newMembership;
  
  public static enum Action {
    ADD, NEW, DELETE, ADD_CONTACT, BACK
  }
  
  @NotNull
  private Action action;
  
}
