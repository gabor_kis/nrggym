package com.webdev.nrggym.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.webdev.nrggym.model.Member;
import com.webdev.nrggym.model.Membership;
import com.webdev.nrggym.service.MemberService;
import com.webdev.nrggym.service.MembershipTypeService;

@Controller
public class ManageMembershipsController {

  private MemberService memberService;
  private MembershipTypeService membershipTypeService;
  
  @Autowired
  public void setMemberService(MemberService memberService) {
    this.memberService = memberService;
  }
  
  @Autowired
  public void setMembershipTypeService(MembershipTypeService membershipTypeService) {
    this.membershipTypeService = membershipTypeService;
  }

  
  @RequestMapping(value = "/manage-memberships", method = RequestMethod.GET)
  public String manageMemberships(Model model, @RequestParam(required = false) Long memberId) {
    model.addAttribute("pageTitle", "EN3RGIE GYM - MANAGER");
    
    Member member = memberId !=null ? memberService.getMemberById(memberId) : new Member();
//    MemberForm form = conversionService.convert(member, MemberForm.class);    
//    model.addAttribute("memberForm", form);
    model.addAttribute("membershipTypes", membershipTypeService.getMembershipTypes());
    model.addAttribute("member", member);
    return "manage-memberships";
  }
  
 
  @RequestMapping(value = "/manage-memberships", method = RequestMethod.POST)
  public String addMembership (
      @ModelAttribute("member") @Valid Member member, BindingResult bindingResult) {
    
    if (bindingResult.hasErrors()) { // if a binding error happened send back the user the form for correction
      // bindingResult.reject("employeeDetails.error.userInput");
      return "/manage-memberships";
    } else {
      Membership m = new Membership();
      m.setType(membershipTypeService.getMembershipTypesById((long)(1)));
      member.getMemberships().add(m);
      
    }    
    
    
    return "redirect:/members";
  }
  
  
  
}
