package com.webdev.nrggym.controller;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.webdev.nrggym.controller.form.MembershipTypeForm;
import com.webdev.nrggym.model.MembershipType;
import com.webdev.nrggym.repository.MembershipTypeRepository;
import com.webdev.nrggym.service.MembershipTypeService;

@Controller
public class MembershipTypeController {

  private MembershipTypeService membershipTypeService;

  private MembershipTypeRepository membershipTypeRepository;

  @Resource
  private ConversionService conversionService;

  @Autowired
  public void setMembershipTypeService(MembershipTypeService membershipTypeService) {
    this.membershipTypeService = membershipTypeService;
  }

  @Autowired
  public void setMembershipTypeRepository(MembershipTypeRepository membershipTypeRepository) {
    this.membershipTypeRepository = membershipTypeRepository;
  }

  @RequestMapping(value = "/memberships", method = RequestMethod.GET)
  public String showMemberships(Model model) {
    model.addAttribute("pageTitle", "MEMBERSHIPS");
    model.addAttribute("memberships", membershipTypeService.getMembershipTypes());
    return "memberships";
  }

  @RequestMapping(value = "/membership", method = RequestMethod.GET)
  public String showMembership(Model model, @RequestParam(required = false) Long membershipId) {
    model.addAttribute("pageTitle", "MEMBERSHIPS");

    MembershipType membership = membershipTypeService.getMembershipTypesById(membershipId);
    MembershipTypeForm form = conversionService.convert(membership, MembershipTypeForm.class);
    model.addAttribute("membershipTypeForm", form);

    return "membership";
  }

  @RequestMapping(value = "/membership", method = RequestMethod.POST)
  public String createOrUpdateMembershipType(
      @ModelAttribute("membershipTypeForm") @Valid MembershipTypeForm membershipTypeForm, BindingResult bindingResult) {

    switch (membershipTypeForm.getAction()) {

    case SAVE:

      if (bindingResult.hasErrors()) { // if a binding error happened send back the user the form for correction
        // bindingResult.reject("employeeDetails.error.userInput");
        return "membership";
      }

      MembershipType membership = conversionService.convert(membershipTypeForm, MembershipType.class);

      try {
        membership = membershipTypeRepository.save(membership);
      } catch (ObjectOptimisticLockingFailureException lockingException) {
        return "membership";
      }
      break;

    case NEW:
      MembershipType newMembership = conversionService.convert(membershipTypeForm, MembershipType.class);
      newMembership.setId(null);
      newMembership = membershipTypeRepository.save(newMembership);
      break;

    case DELETE:
      membershipTypeRepository.delete(membershipTypeForm.getMembershipType());
      break;

    }
    return "redirect:/memberships";
  }

}
