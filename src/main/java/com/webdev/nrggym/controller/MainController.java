package com.webdev.nrggym.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.convert.ConversionService;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.webdev.nrggym.controller.form.MemberForm;
import com.webdev.nrggym.model.ContactInfo;
import com.webdev.nrggym.model.Member;
import com.webdev.nrggym.model.Membership;
import com.webdev.nrggym.model.MembershipType;
import com.webdev.nrggym.repository.MemberRepository;
import com.webdev.nrggym.repository.MembershipTypeRepository;
import com.webdev.nrggym.service.MemberService;
import com.webdev.nrggym.service.MembershipTypeService;

@Controller

public class MainController {

  private MemberService memberService;
  private MemberRepository memberRepository;
  private MembershipTypeService membershipTypeService;
  private MembershipTypeRepository membershipTypeRepository;
  
  @Resource
  private ConversionService conversionService;
  
  @Autowired
  public void setMemberService(MemberService memberService) {
    this.memberService = memberService;
  }
  
  @Autowired
  public void setMemberRepository(MemberRepository memberRepository) {
    this.memberRepository = memberRepository;
  }

  @Autowired
  public void setMembershipTypeService(MembershipTypeService membershipTypeService) {
    this.membershipTypeService = membershipTypeService;
  }  
  
  @Autowired
  public void setMembershipTypeRepository(MembershipTypeRepository membershipTypeRepository) {
    this.membershipTypeRepository = membershipTypeRepository;
  }
  
  @Cacheable("membershipTypes")
  @ModelAttribute("membershipTypes")
  public Iterable<MembershipType> provideMembershipType(){
    return membershipTypeRepository.findAll();
  }

  @RequestMapping(value = "/members", method = RequestMethod.GET)
  public String showIndex(Model model) {
    model.addAttribute("pageTitle", "EN3RGIE GYM");
    model.addAttribute("members", memberService.getMembers());
    return "members";
  }
  
  @RequestMapping(value = "/member", method = RequestMethod.GET)
  public String showMember(Model model, @RequestParam(required = false) Long memberId) {
    model.addAttribute("pageTitle", "EN3RGIE GYM-MEMBER");
    
    Member member = memberId !=null ? memberService.getMemberById(memberId) : new Member();
    MemberForm form = conversionService.convert(member, MemberForm.class);
    
    model.addAttribute("memberForm", form);
    
    return "member";
  }
  
  @RequestMapping(value = "/member", method = RequestMethod.POST)
  public String createUpdateDeleteMember (
      @ModelAttribute("memberForm") @Valid MemberForm memberForm, BindingResult bindingResult) {
    
    if (memberForm.getAction() != null) {
      
    
      switch (memberForm.getAction()) {
      
        case SAVE:      
            if (bindingResult.hasErrors()) { // if a binding error happened send back the user the form for correction
              bindingResult.reject("error filled");
              return "member";
            }        
            Member member = conversionService.convert(memberForm, Member.class);
            
            try {
              member = memberRepository.save(member);
              
            }catch(ObjectOptimisticLockingFailureException lockingException) {
              //bindingResult.reject("error");
              return "member";
            }        
            break;
            
        case DELETE:
          
          memberRepository.delete(memberForm.getMember());
          break;
         
        case ADD_CONTACT:      
          memberForm.getMember().getContactInfos().add(new ContactInfo());
          return "member"; 
          
        case ADD_MEMBERSHIP:      
          memberForm.getMember().getMemberships().add(new Membership());         
          return "member";   
          
        case BACK:
          break;
      }
      
  } else {
    
    deleteEntry(memberForm.getMember().getContactInfos(), memberForm.getDeleteContactInfoIndex());
    memberForm.setDeleteContactInfoIndex(null);
    
    deleteEntry(memberForm.getMember().getMemberships(), memberForm.getDeleteMembershipInfoIndex());
    memberForm.setDeleteContactInfoIndex(null);
    
    return "member";
  }  
    return "redirect:/members";
}  
  
  private <T> void deleteEntry(List<T> list, Integer indexOfElementToDelete) {
    if (indexOfElementToDelete != null && indexOfElementToDelete <= list.size()) {
        list.remove((int) indexOfElementToDelete);
    }  
  }
  
  @RequestMapping(value = "/main", method = RequestMethod.GET)
  public String showMainPage(Model model, @RequestParam(required = false) Long id) {    
    model.addAttribute("pageTitle", "EN3RGIE GYM-MANAGE");
    String message = "";
    Member member = id !=null ? memberService.getMemberById(id) : new Member();
    if (member == null) {
      member = new Member();
      message = "There is no such member id in the database!";
    }
    model.addAttribute("member", member);
    model.addAttribute("message", message);
    return "main";
  }
  
  
}
