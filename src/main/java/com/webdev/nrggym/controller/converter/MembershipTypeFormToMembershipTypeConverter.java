package com.webdev.nrggym.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.webdev.nrggym.controller.form.MembershipTypeForm;
import com.webdev.nrggym.model.MembershipType;

@Component
public class MembershipTypeFormToMembershipTypeConverter implements Converter<MembershipTypeForm, MembershipType> {

  @Override
  public MembershipType convert(MembershipTypeForm source) {
    return source.getMembershipType();
  }
    
  
}
