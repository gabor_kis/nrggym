package com.webdev.nrggym.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.webdev.nrggym.controller.form.MemberForm;
import com.webdev.nrggym.model.Member;

@Component
public class MemberFormToMemberConverter implements Converter<MemberForm, Member> {

  @Override
  public Member convert(MemberForm source) {
    return source.getMember();
  }

}
