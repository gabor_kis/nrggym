package com.webdev.nrggym.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.webdev.nrggym.controller.form.MembershipTypeForm;
import com.webdev.nrggym.model.MembershipType;

@Component
public class MembershipTypeToMembershipTypeForm implements Converter<MembershipType, MembershipTypeForm>{
  
  @Override
  public MembershipTypeForm convert(MembershipType membershipType) {
    
    MembershipTypeForm form = new MembershipTypeForm();
    form.setMembershipType(membershipType);
    
    return form;
  }

 
  

}
