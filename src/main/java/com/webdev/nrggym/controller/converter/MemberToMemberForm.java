package com.webdev.nrggym.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.webdev.nrggym.controller.form.MemberForm;
import com.webdev.nrggym.model.Member;

@Component
public class MemberToMemberForm implements Converter<Member, MemberForm> {

  @Override
  public MemberForm convert(Member member) {

    MemberForm form = new MemberForm();
    form.setMember(member);
    
    return form;    
  }

  
}
