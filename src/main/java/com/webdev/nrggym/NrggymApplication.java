package com.webdev.nrggym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class NrggymApplication {

	public static void main(String[] args) {
		SpringApplication.run(NrggymApplication.class, args);
	}
}
