package com.webdev.nrggym.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.webdev.nrggym.model.Member;

public interface MemberRepository extends CrudRepository<Member, Long> {

  List<Member> findAll();
  Member findByLastName(String lastName);
  Member findById(Long id);
  
}
