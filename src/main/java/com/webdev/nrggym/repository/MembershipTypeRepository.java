package com.webdev.nrggym.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.webdev.nrggym.model.MembershipType;

public interface MembershipTypeRepository extends CrudRepository<MembershipType, Long> {
  
  List<MembershipType> findAll();
  
  MembershipType findByName(String name);
  
  MembershipType findById(Long id);

}
